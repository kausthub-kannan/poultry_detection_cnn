from fastapi import FastAPI
import tensorflow as tf
import tensorflow_hub as hub
from fastapi.middleware.cors import CORSMiddleware
import base64
import numpy as np
from PIL import Image
import io
from starlette.requests import Request

poultry_diseases_list = ["Coccidiosis", "Healthy", "New Castle Diseases", "Salmonella"]


def model_predict(encoded_data, model, image_shape=224, channels=3, norm_factor=255.):
    """Base64 to Numpy Decoding"""
    decoded_image = base64.b64decode(encoded_data[23:])
    img = Image.open(io.BytesIO(decoded_image))
    width, height = img.size
    img_array = np.array(img)
    img = np.reshape(img_array, (height, width, 3))

    """Pre-processing for the Model"""
    img = tf.image.resize(img, size=(image_shape, image_shape))
    img = tf.expand_dims(img, axis=0)
    img = img / norm_factor

    '''Diseases Class Prediction'''
    pred = model.predict(img)
    prediction = tf.math.round(pred).numpy()
    print(prediction)
    prediction = prediction.argmax(axis=1)
    print(prediction)
    prediction = prediction.item()
    return prediction


app = FastAPI()
hub_model = hub.KerasLayer("https://tfhub.dev/google/imagenet/mobilenet_v2_100_224/feature_vector/5")
tf.keras.utils.register_keras_serializable('KerasLayer')(hub.KerasLayer)
poultry_model = tf.keras.models.load_model("model/poultry.h5", custom_objects={'KerasLayer': hub.KerasLayer})

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def root():
    return "API to find plant and poultry diseases using well-built Conventional Neural Network Models"


@app.post("/predictions/poultry_diseases")
async def poultry_diseases(base64_str: Request):
    try:
        print(type(base64_str))
        image = await base64_str.json()
        return {"prediction": poultry_diseases_list[model_predict(image, poultry_model)]}
    except Exception as e:
        print(e)
        return {"error": str(e)}
