fastapi==0.95.1
numpy==1.21.6
Pillow==9.5.0
Requests==2.30.0
starlette==0.26.1
tensorflow==2.11.0
tensorflow_hub==0.13.0
uvicorn
